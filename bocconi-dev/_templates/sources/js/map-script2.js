(function($, win, doc, undefined){

  var $html = $('html'),
      $body = $('body'),
      $window = $(window),
      $document  = $(document),

      jasonurl = "data/chapters.json";

  readJson = function() {
    $.getJSON(jasonurl, function(data) {
      $.each(data.europe,function(i,v){
        $.each(v,function(country,v){
          ///console.log(country,v)
          $.each(v,function(i,city){
            //console.log(i,city)
            $.each(city,function(fieldName,value){
              //console.log(fieldName,value)
            })
          })
        })
      })
    })
  }

  appendCirclesWorld = function () {
    var json_w = $.parseJSON(world);
    var cDim = 0;
    var indexC = 0;
    for (var i in json_w) {
      //console.log(json_obj[i].value);
      //$(".map-layer--world").append("<div class='circle'></div>")
      $(".map-layer.map-layer--world").append("<a data-map='"+ json_w[i].id+"' data-name='"+ json_w[i].name+"' href='javascript:;' class='circle open-country' style='left: "+ json_w[i].posx+"%; top: "+ json_w[i].posy+"%;'><label>"+ json_w[i].value +"</label></a>")
      cDim = json_w[i].value * 7;
      indexC = i + 1;
      $(".map-layer.map-layer--world .circle").eq(i).css("width",cDim+"px");
      $(".map-layer.map-layer--world .circle").eq(i).css("height",cDim+"px");
      if ( cDim >= 10 ) {
        $(".map-layer.map-layer--world .circle").eq(i).css("line-height", cDim + "px");
      }
    }
  }

  appendCirclesContinent = function () {
    var cDim = 0;
    var indexC = 0;
    var json_c = $.parseJSON(italy);

    for (var i in json_c) {
      cDim = json_c[i].value * 7;
      indexC = i + 1;
      $(".map-layer.map-layer--country").append("<a data-map='"+ json_c[i].id+"' data-name='"+ json_c[i].name+"' href='javascript:;' class='circle open-detail active' style='left: "+ json_c[i].posx+"%; top: "+ json_c[i].posy+"%;'><label>"+ json_c[i].value +"</label><div id='modale'></div></a>")
      $(".map-layer.map-layer--country .circle").eq(i).css("width",cDim+"px");
      $(".map-layer.map-layer--country .circle").eq(i).css("height",cDim+"px");
      if ( cDim >= 10 ) {
        $(".map-layer.map-layer--country .circle").eq(i).css("line-height",cDim+"px");
      }
    }
  }

  appendCirclesCountry = function () {
    var cDim = 0;
    var indexC = 0;
    var json_c = $.parseJSON(italy);

    for (var i in json_c) {
      cDim = json_c[i].value * 7;
      indexC = i + 1;
      $(".map-layer.map-layer--country").append("<a data-map='"+ json_c[i].id+"' data-name='"+ json_c[i].name+"' href='javascript:;' class='circle open-detail active' style='left: "+ json_c[i].posx+"%; top: "+ json_c[i].posy+"%;'><label>"+ json_c[i].value +"</label><div id='modale'></div></a>")
      $(".map-layer.map-layer--country .circle").eq(i).css("width",cDim+"px");
      $(".map-layer.map-layer--country .circle").eq(i).css("height",cDim+"px");
      if ( cDim >= 10 ) {
        $(".map-layer.map-layer--country .circle").eq(i).css("line-height",cDim+"px");
      }
    }
  }

  goToContinent = function () {
    $(".map-layer--world .open-continent").click(
        function () {
          var mapName = $(this).attr("data-name");
          var mapId = $(this).attr("data-map");
          if ( mapId == 2 ) {
            var imageUrl = "img/continent/"+ mapName +".svg";
            $(".map-layer--continent").css('background-image', 'url(' + imageUrl + ')');
            $(".map-layer--world").addClass("hidden");
            $(".map-layer--continent").removeClass("hidden");
            $(".map-layer--continent").addClass("fadeIn animated");
            $(".viewallmap").addClass("hide");
            $(".backmap").removeClass("hide");
            appendCirclesContinent();
          }
        }
    )
  },

      goToCountry = function () {
        $(".map-layer--world .open-country").click(
            function () {
              var mapName = $(this).attr("data-name");
              var mapId = $(this).attr("data-map");
              if ( mapId == 2 ) {
                var imageUrl = "img/countries/"+ mapName +".svg";
                $(".map-layer--country").css('background-image', 'url(' + imageUrl + ')');
                $(".map-layer--world").addClass("hidden");
                $(".map-layer--country").removeClass("hidden");
                $(".map-layer--country").addClass("fadeIn animated");
                $(".viewallmap").addClass("hide");
                $(".backmap").removeClass("hide");
                appendCirclesCountry();
              }
            }
        )
      },

      opendetail = function () {
        $("body").delegate(".map-layer--country .open-detail.active", "click", function(){
              var mapId = $(this).attr("data-map");
              var json_c = $.parseJSON(italy);
              if ( mapId == 1 ) {
                var imgUrl = 'img/'+json_c[mapId-1].name+'.png';
                var htmlModal = '<div class="map-modal card-list"><div class="card card--event"><div class="image-wrap" style="background-image: url('+ imgUrl +')"></div><div class="event-content-wrap"><div class="green smalltitle text-uppercase mb-1">chapter '+ json_c[mapId-1].name +'</div><div class="gray midtext pb-05">Chapter Leader:<b>'+ json_c[mapId-1].leader +'</b></div><div class="gray midtext mb-2">Email:<b>'+ json_c[mapId-1].email +'</b></div><div class="gray midtext">'+ json_c[mapId-1].text +'</div></div></div></div>';
                var modalClose = '<a class="modal-close" href="javascript:;"><span class="icon icon-close"></span></a>'
                $(this).find("#modale").append(htmlModal);
                $(this).append(modalClose);
                $(this).removeClass("active");
              }
            }
        )
      },

      backmap = function() {
        $("body").delegate(".backmap", "click", function(){
          console.log("b");
          $(".map-layer--country").addClass("hidden");
          $(".map-layer--world").removeClass("hidden");
          $(".map-layer--world").addClass("fadeIn animated");
          $(".backmap").addClass("hide");
          $(".viewallmap").removeClass("hide");
        })
      },

      closeModal = function () {
        $("body").delegate(".modal-close", "click", function(){
          $("#modale").html('');
          $(".modal-close").remove();
          $(".map-layer--country .open-detail").addClass("active");
        })
      },

      $document
          .ready(function() {
            readJson();
            //appendCirclesWorld();
            //goToCountry();
            //opendetail();
            //closeModal();
            //backmap();
          });

})(jQuery, window, document);