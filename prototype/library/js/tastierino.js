var $numInput;

$(function() {
  initKeyboard($('#numeric-keyboard'), $('#num-input'));
  
  $('#num-input').on('keydown', function(e) {
    $('#event').text('key: '+e.keyCode);
  });
  
  $('#back').on('click', onBackBtn);
  $('#forward').on('click', onForwardBtn);
});

function onBackBtn() {
   $('input').first().focus(); 
}

function onForwardBtn() {
   $('input').last().focus(); 
}

function initKeyboard($keyboard, $input) {
  $numInput = $input;
  
   $keyboard.on('click', '.key', onKeyPress); 
}

function onKeyPress(e) {
  var keyValue = $(this).attr('data-value');
  
  if(keyValue.toLowerCase() === 'delete') {
     keyValue = 8; 
  } else if(keyValue === '') {
     return; 
  } else {
     keyValue = keyValue.charCodeAt(0); 
  }
  
  var e = $.Event("keydown");
  e.which = keyValue;
  e.keyCode = keyValue;
  
  $numInput.trigger(e);
  
  if(keyValue !== 8) {
    $numInput.val( $numInput.val() + $(this).text() );
  } else {
    $numInput.val( $numInput.val().slice(0, -1) );
  }
}